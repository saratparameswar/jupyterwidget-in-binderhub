FROM python:3.8-slim

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --no-cache --upgrade pip && \
    pip install --no-cache notebook \
    pip install npm \
    pip install --no-cache jupyterlab \
    pip install jupyter-contrib-core \
    pip install jupyter-contrib-nbextensions\
    pip install ipywidgets \
    pip install --upgrade jupyter_client && \
    # ipywidgets nbextension
    jupyter nbextension enable --py widgetsnbextension --sys-prefix && \
    pip install mstrio-py==11.3.3.101 && \
    jupyter nbextension install connector-jupyter --py --sys-prefix && \
    jupyter nbextension enable connector-jupyter --py --sys-prefix
    
RUN pip install ipyleaflet
RUN jupyter nbextension enable --py --sys-prefix ipyleaflet


ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

COPY . ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}


